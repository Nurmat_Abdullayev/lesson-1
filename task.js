let text = prompt("type some text!")
let words = text.split(' ')
let changedText = null

for (let i = 0; i < words.length; i++) {
	words[i] = words[i].toLowerCase()
	let letters = words[i].split('')
	letters[0] = letters[0].toUpperCase()
	words[i] = letters.join('')
}
changedText = words.join(' ')
document.write(changedText)
